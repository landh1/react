import React from "react";

function Page1Content() {
  return (
    <div>
      <h2>Version and Usage - Page 1</h2>
      <p>This is Page 1 content.</p>
    </div>
  );
}

export default Page1Content;
