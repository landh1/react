import React, { useState } from 'react';
import HomePage from './HomePage';
import Page1 from './pages/Page1';
import Page2 from './pages/Page2';
import Page3 from './pages/Page3'; // Add Page3
import Page4 from './pages/Page4'; // Add Page4
import Page5 from './pages/Page5'; // Add Page5
import './App.css'; // Import CSS file for styling

function App() {
  const [currentPage, setCurrentPage] = useState('home');

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderPage = () => {
    switch (currentPage) {
      case 'home':
        return <HomePage />;
      case 'page1':
        return <Page1 />;
      case 'page2':
        return <Page2 />;
      case 'page3':
        return <Page3 />;
      case 'page4':
        return <Page4 />;
      case 'page5':
        return <Page5 />;
      default:
        return <HomePage />;
    }
  };

  return (
    <div className="app-container">
      <div className="left-pane">
        <h2>Navigation Menu</h2>
        <ul>
          <li onClick={() => handlePageChange('home')} className={currentPage === 'home' ? 'active' : ''}>Home</li>
          <li onClick={() => handlePageChange('page1')} className={currentPage === 'page1' ? 'active' : ''}>Page 1</li>
          <li onClick={() => handlePageChange('page2')} className={currentPage === 'page2' ? 'active' : ''}>Page 2</li>
          <li onClick={() => handlePageChange('page3')} className={currentPage === 'page3' ? 'active' : ''}>Page 3</li>
          <li onClick={() => handlePageChange('page4')} className={currentPage === 'page4' ? 'active' : ''}>Page 4</li>
          <li onClick={() => handlePageChange('page5')} className={currentPage === 'page5' ? 'active' : ''}>Page 5</li>
        </ul>
      </div>
      <div className="main-content">
        {renderPage()}
      </div>
    </div>
  );
}

export default App;
