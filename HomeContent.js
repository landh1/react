import React from "react";

function HomeContent() {
  return (
    <div>
      <h2> Why Ted ? </h2>
      <p>
        The Technical Expertise Dashboard (TED) serves as a centralized hub for
        monitoring and analyzing GitLab Pipeline job data, encompassing crucial
        scans such as Nexus IQ reports, SonarQube scan reports, and Fortify
        results. This comprehensive overview offers numerous benefits for teams
        and organizations invested in maintaining high-quality software
        development practices.
        <br />
        Firstly, TED provides real-time insights into the health and security of
        software projects, empowering teams to promptly address any detected
        vulnerabilities or code quality issues. By aggregating data from various
        scans, it enables efficient identification of trends, patterns, and
        potential areas for improvement across multiple projects.
        <br /> Moreover, TED enhances collaboration and decision-making by
        offering a unified platform for cross-team visibility and
        accountability. Project stakeholders, including developers, QA
        engineers, and management, can access relevant scan reports and metrics,
        fostering transparency and alignment towards common goals.
        <br />
        Furthermore, TED supports proactive risk management and compliance
        efforts by facilitating the monitoring of code quality standards,
        security vulnerabilities, and regulatory requirements. This proactive
        approach helps mitigate potential risks early in the development
        lifecycle, reducing the likelihood of costly rework or security breaches
        later on.
        <br />
        Overall, TED streamlines the monitoring and analysis of GitLab Pipeline
        job data, enabling teams to proactively enhance software quality,
        security, and compliance while fostering collaboration and informed
        decision-making across the organization.
      </p>
    </div>
  );
}

export default HomeContent;
