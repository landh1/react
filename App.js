// App.js
import React, { useState } from "react";
import "./App.css";
import HomeContent from "./pages/HomeContent";
import Page1Content from "./pages/Page1Content";
import Page2Content from "./pages/Page2Content";

function App() {
  const [currentPage, setCurrentPage] = useState("HOME");

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <div className="app">
      <div className="sidebar">
        <h2 className="dashboard-title">TED DASHBOARD</h2>
        <div className="menu-items">
          <div
            className={`menu-item ${currentPage === "HOME" ? "active" : ""}`}
            onClick={() => handlePageChange("HOME")}
          >
            HOME
          </div>
          <div
            className={`menu-item ${currentPage === "Page 1" ? "active" : ""}`}
            onClick={() => handlePageChange("Page 1")}
          >
            PAGE 1
          </div>
          <div
            className={`menu-item ${currentPage === "Page 2" ? "active" : ""}`}
            onClick={() => handlePageChange("Page 2")}
          >
            PAGE 2
          </div>
        </div>
      </div>
      <div className="main-content">
        <h1 className="page-title">{currentPage}</h1>
        <div className="page-content">
          {/* Content for each page goes here */}
          {currentPage === "HOME" && <HomeContent />}
          {currentPage === "Page 1" && <Page1Content />}
          {currentPage === "Page 2" && <Page2Content />}
        </div>
      </div>
    </div>
  );
}

export default App;
